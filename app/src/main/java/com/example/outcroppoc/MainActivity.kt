package com.example.outcroppoc

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Point
import android.net.Uri
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.get
import androidx.exifinterface.media.ExifInterface
import com.example.outcroppoc.databinding.ActivityMainBinding
import java.io.FileNotFoundException
import java.io.IOException
import java.io.InputStream
import android.view.GestureDetector
import android.view.GestureDetector.SimpleOnGestureListener
import android.view.ScaleGestureDetector
import android.view.animation.ScaleAnimation
import kotlin.math.pow

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var exif: ExifInterface
    private lateinit var imageUri: Uri
    private var distancePointOne: Point? = null
    private var distancePointTwo: Point? = null
    private var referencePointOne: Point? = null
    private var referencePointTwo: Point? = null

    private var mScale = 1f
    private var mScaleGestureDetector: ScaleGestureDetector? = null
    var gestureDetector: GestureDetector? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setupListeners()
        gestureDetector = GestureDetector(this, GestureListener())
        mScaleGestureDetector = ScaleGestureDetector(this, object : ScaleGestureDetector.SimpleOnScaleGestureListener() {
            override fun onScale(detector: ScaleGestureDetector): Boolean {
                val scale = 1 - detector.scaleFactor
                val prevScale: Float = mScale
                mScale += scale

                // we can maximise our focus to 10f only
                if (mScale > 10f) mScale = 10f
                val scaleAnimation = ScaleAnimation(
                    1f / prevScale,
                    1f / mScale,
                    1f / prevScale,
                    1f / mScale,
                    detector.focusX,
                    detector.focusY
                )

                scaleAnimation.duration = 0
                scaleAnimation.fillAfter = true
                binding.myscroll.startAnimation(scaleAnimation)
                return true
            }
        })
    }

    override fun dispatchTouchEvent(event: MotionEvent?): Boolean {
        super.dispatchTouchEvent(event)
        mScaleGestureDetector!!.onTouchEvent(event)
        gestureDetector!!.onTouchEvent(event)
        return gestureDetector!!.onTouchEvent(event)
    }

    private class GestureListener : SimpleOnGestureListener() {
        override fun onDown(e: MotionEvent): Boolean {
            return true
        }

        override fun onDoubleTap(e: MotionEvent): Boolean {
            return true
        }
    }

    @SuppressLint("ClickableViewAccessibility", "SetTextI18n")
    private fun setupListeners() {
        binding.apply {
            topAppBar.setOnMenuItemClickListener { menuItem ->
                topAppBar.menu[0].iconTintList = this@MainActivity.getColorStateList(R.color.gray)
                topAppBar.menu[1].iconTintList = this@MainActivity.getColorStateList(R.color.gray)
                topAppBar.menu[2].iconTintList = this@MainActivity.getColorStateList(R.color.gray)
                topAppBar.menu[3].iconTintList = this@MainActivity.getColorStateList(R.color.gray)
                when (menuItem.itemId) {
                    R.id.polygon -> {
                        menuItem.iconTintList = this@MainActivity.getColorStateList(R.color.yellow_100)
                        binding.pictureTransparentLayer.setCurrentAction(Enums.Actions.DRAW_POLYGON)
                        measurementContainer.visibility = GONE
                        true
                    }
                    R.id.line -> {
                        menuItem.iconTintList = this@MainActivity.getColorStateList(R.color.yellow_100)
                        binding.pictureTransparentLayer.setCurrentAction(Enums.Actions.DRAW_LINE)
                        measurementContainer.visibility = GONE
                        true
                    }
                    R.id.point -> {
                        menuItem.iconTintList = this@MainActivity.getColorStateList(R.color.yellow_100)
                        binding.pictureTransparentLayer.setCurrentAction(Enums.Actions.DRAW_POINT)
                        measurementContainer.visibility = GONE
                        true
                    }
                    R.id.measure -> {
                        menuItem.iconTintList = this@MainActivity.getColorStateList(R.color.yellow_100)
                        binding.pictureTransparentLayer.setCurrentAction(Enums.Actions.MEASURE)
                        measurementContainer.visibility = VISIBLE
                        true
                    }
                    else -> false
                }

            }
            takeFromGalleryButton.setOnClickListener {
                val photoPickerIntent = Intent(Intent.ACTION_PICK)
                photoPickerIntent.type = "image/*"
                startActivityForResult(photoPickerIntent, 123)
                takeFromGalleryButton.visibility = View.GONE
            }
            changeExifData.setOnClickListener {
                exif.setLatLong(12.0, 11.0)
                exif.setAttribute(ExifInterface.TAG_MODEL, "new model")
                exif.setAttribute(ExifInterface.TAG_USER_COMMENT, "\"{\\\"widget\\\": {\\n    \\\"debug\\\": \\\"on\\\",\\n    \\\"window\\\": {\\n        \\\"title\\\": \\\"Sample Konfabulator Widget\\\",\\n        \\\"name\\\": \\\"main_window\\\",\\n        \\\"width\\\": 500,\\n        \\\"height\\\": 500\\n    },\\n    \\\"image\\\": { \\n        \\\"src\\\": \\\"Images\\/Sun.png\\\",\\n        \\\"name\\\": \\\"sun1\\\",\\n        \\\"hOffset\\\": 250,\\n        \\\"vOffset\\\": 250,\\n        \\\"alignment\\\": \\\"center\\\"\\n    },\\n    \\\"text\\\": {\\n        \\\"data\\\": \\\"Click Here\\\",\\n        \\\"size\\\": 36,\\n        \\\"style\\\": \\\"bold\\\",\\n        \\\"name\\\": \\\"text1\\\",\\n        \\\"hOffset\\\": 250,\\n        \\\"vOffset\\\": 100,\\n        \\\"alignment\\\": \\\"center\\\",\\n        \\\"onMouseUp\\\": \\\"sun1.opacity = (sun1.opacity \\/ 100) * 90;\\\"\\n    }\\n}}\"")
                displayExif(exif)
            }

            getExifData.setOnClickListener {
                getExiFileData(imageUri)
                changeExifData.visibility = View.VISIBLE
                exifDataText.visibility = View.VISIBLE
                getExifData.visibility = View.GONE
            }

            slider.addOnChangeListener { _, value, _ ->
                imageView.pivotX = pictureTransparentLayer.selectedX
                imageView.pivotY = pictureTransparentLayer.selectedY
                pictureTransparentLayer.pivotX = pictureTransparentLayer.selectedX
                pictureTransparentLayer.pivotY = pictureTransparentLayer.selectedY
                imageView.scaleX = value / 100
                imageView.scaleY = value / 100
                pictureTransparentLayer.scaleX = value / 100
                pictureTransparentLayer.scaleY = value / 100
                imageView.requestLayout()
                pictureTransparentLayer.requestLayout()
                pictureTransparentLayer.setZoom(value)
            }
            topAppBar.menu.performIdentifierAction(R.id.polygon, 0)
            topAppBar.menu.setGroupVisible(R.id.menu_group, false)

            imageView.setOnTouchListener { view, motionEvent ->
                if(motionEvent.action != MotionEvent.ACTION_UP) {
                    return@setOnTouchListener true
                }

                if(binding.pictureTransparentLayer.action != Enums.Actions.MEASURE) {
                    return@setOnTouchListener true
                }

                if(referencePointOne == null) {
                    referencePointOne = Point(motionEvent.x.toInt(), motionEvent.y.toInt())
                    binding.pictureTransparentLayer.addPointToPointsList(referencePointOne!!)
                    return@setOnTouchListener true
                }

                if(referencePointTwo == null) {
                    referencePointTwo = Point(motionEvent.x.toInt(), motionEvent.y.toInt())
                    binding.pictureTransparentLayer.addPointToPointsList(referencePointTwo!!)
                    return@setOnTouchListener true
                }

                if(distancePointOne == null) {
                    distancePointOne = Point(motionEvent.x.toInt(), motionEvent.y.toInt())
                    binding.pictureTransparentLayer.addPointToPointsList(distancePointOne!!)
                    return@setOnTouchListener true
                }

                if(distancePointTwo == null) {
                    distancePointTwo = Point(motionEvent.x.toInt(), motionEvent.y.toInt())
                    binding.pictureTransparentLayer.addPointToPointsList(distancePointTwo!!)
                    return@setOnTouchListener true
                }

                if(measurement.text.isEmpty() && distance.text.isNotEmpty()) {
                    measurement.text = (distance.text.toString().toDouble() * distanceBetweenPoints(distancePointOne!!, distancePointTwo!!) / distanceBetweenPoints(referencePointOne!!, referencePointTwo!!)).toString()
                    return@setOnTouchListener true
                }

                if (measurement.text.isNotEmpty()) {
                    binding.pictureTransparentLayer.clearPointToPointsList()
                    referencePointOne = null
                    referencePointTwo = null
                    distancePointOne = null
                    distancePointTwo = null
                    measurement.text = ""
                    distance.text.clear()
                    return@setOnTouchListener true
                }

                return@setOnTouchListener true
            }


        }
    }

    private fun distanceBetweenPoints(a: Point, b: Point): Double {
        return kotlin.math.sqrt((a.x - b.x).toDouble().pow(2) + (a.y - b.y).toDouble().pow(2))
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        data?.apply {
            if (resultCode == RESULT_OK) {
                try {
                    imageUri = this.data!!
                    val imageStream: InputStream? = contentResolver.openInputStream(imageUri)
                    val selectedImage = BitmapFactory.decodeStream(imageStream)
                    binding.imageView.setImageBitmap(selectedImage)
                    binding.imageView.layoutParams.apply {
                        width = selectedImage.width
                        height = selectedImage.height
                    }

                    binding.pictureTransparentLayer.apply {
                        visibility = View.VISIBLE
                        layoutParams.apply {
                            height = selectedImage.height
                            width = selectedImage.width
                        }
                    }
                    binding.slider.visibility = View.VISIBLE
                    binding.zoomText.visibility = View.VISIBLE
                    binding.imageView.requestLayout()
                    binding.getExifData.visibility = View.VISIBLE
                    imageStream?.close()
                    binding.changeExifData.visibility = View.GONE
                    binding.topAppBar.menu.setGroupVisible(R.id.menu_group, true)
                } catch (e: FileNotFoundException) {
                    e.printStackTrace()
                    Toast.makeText(this@MainActivity, "Something went wrong", Toast.LENGTH_LONG)
                        .show()
                }
            } else {
                Toast.makeText(this@MainActivity, "You haven't picked Image", Toast.LENGTH_LONG)
                    .show()
            }
        }
    }

    private fun getExiFileData(uri: Uri) {
        try {
            contentResolver.openInputStream(uri).use { inputStream ->
                exif = ExifInterface(inputStream!!)
                displayExif(exif)
                inputStream.close()
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    private fun displayExif(exif: ExifInterface) {
        val builder = StringBuilder()
        builder.apply {
            append(
                "Date & Time: " + getExifTag(
                    exif,
                    ExifInterface.TAG_DATETIME
                ) + "\n"
            )
            append("Flash: " + getExifTag(exif, ExifInterface.TAG_FLASH) + "\n")
            append(
                "Focal Length: " + getExifTag(
                    exif,
                    ExifInterface.TAG_FOCAL_LENGTH
                ) + "\n"
            )
            append(
                "GPS Datestamp: " + getExifTag(
                    exif,
                    ExifInterface.TAG_FLASH
                ) + "\n"
            )
            append(
                "GPS Latitude: " + getExifTag(
                    exif,
                    ExifInterface.TAG_GPS_LATITUDE
                ) + "\n"
            )
            append(
                "GPS Latitude Ref: " + getExifTag(
                    exif,
                    ExifInterface.TAG_GPS_LATITUDE_REF
                ) + "\n"
            )
            append(
                "GPS Longitude: " + getExifTag(
                    exif,
                    ExifInterface.TAG_GPS_LONGITUDE
                ) + "\n"
            )
            append(
                "GPS Longitude Ref: " + getExifTag(
                    exif,
                    ExifInterface.TAG_GPS_LONGITUDE_REF
                ) + "\n"
            )
            append(
                "GPS Processing Method: " + getExifTag(
                    exif,
                    ExifInterface.TAG_GPS_PROCESSING_METHOD
                ) + "\n"
            )
            append(
                "GPS Timestamp: " + getExifTag(
                    exif,
                    ExifInterface.TAG_GPS_TIMESTAMP
                ) + "\n"
            )
            append(
                "Image Length: " + getExifTag(
                    exif,
                    ExifInterface.TAG_IMAGE_LENGTH
                ) + "\n"
            )
            append(
                "Image Width: " + getExifTag(
                    exif,
                    ExifInterface.TAG_IMAGE_WIDTH
                ) + "\n"
            )
            append(
                "Camera Make: " + getExifTag(
                    exif,
                    ExifInterface.TAG_MAKE
                ) + "\n"
            )
            append(
                "Camera Model: " + getExifTag(
                    exif,
                    ExifInterface.TAG_MODEL
                ) + "\n"
            )
            builder.append(
                "Camera Orientation: " + getExifTag(
                    exif,
                    ExifInterface.TAG_ORIENTATION
                ) + "\n"
            )
            append(
                "Camera White Balance: " + getExifTag(
                    exif,
                    ExifInterface.TAG_WHITE_BALANCE
                ) + "\n"
            )
            append(
                "User comment: " + getExifTag(
                    exif,
                    ExifInterface.TAG_USER_COMMENT
                ) + "\n"
            )

        }
        binding.exifData.text = builder.toString()
    }

    private fun getExifTag(exif: ExifInterface, tag: String): String {
        val attribute = exif.getAttribute(tag)
        return attribute ?: ""
    }
}
