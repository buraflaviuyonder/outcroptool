package com.example.outcroppoc

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import com.example.outcroppoc.Enums.Actions.*

class PaintForCameraPicture(context: Context?, attrs: AttributeSet?) : View(context, attrs) {

    private var listOfPolygonPoints = mutableListOf<Point>()
    private var listOfLinesPoints = mutableListOf<Point>()
    private var listOfPointsPoints = mutableListOf<Point>()
    var action = DRAW_POLYGON
    private lateinit var pictureCanvas: Canvas
    var selectedX = 0f
    var selectedY = 0f
    var zoomLevel = 1f

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        canvas?.let {
            pictureCanvas = it
            val mPathPolygon = Path()
            val mPathLine = Path()

            val zoomlvl = 5F - zoomLevel/120
            val nodeColor = Paint(Paint.ANTI_ALIAS_FLAG)
            nodeColor.color = Color.GREEN
            val mPolygonPaint = Paint(Paint.ANTI_ALIAS_FLAG)
            mPolygonPaint.color = Color.BLUE
            mPolygonPaint.strokeWidth = zoomlvl
            mPolygonPaint.style = Paint.Style.FILL_AND_STROKE

            val mLinePaint = Paint(Paint.ANTI_ALIAS_FLAG)
            mLinePaint.color = Color.RED
            mLinePaint.strokeWidth = zoomlvl
            mLinePaint.style = Paint.Style.STROKE

            val mPointPaint = Paint()
            mPointPaint.color = Color.GREEN
            mPointPaint.style = Paint.Style.FILL

        if (!listOfPolygonPoints.isNullOrEmpty()) {
            val startingValue = listOfPolygonPoints[0]
            mPathPolygon.moveTo(startingValue.x.toFloat(), startingValue.y.toFloat())
            for (point in listOfPolygonPoints) {
                mPathPolygon.lineTo(point.x.toFloat(), point.y.toFloat())
                canvas?.drawCircle(point.x.toFloat(), point.y.toFloat(), zoomlvl, nodeColor)
            }

                canvas?.drawLine(
                    startingValue.x.toFloat(),
                    startingValue.y.toFloat(),
                    listOfPolygonPoints.last().x.toFloat(),
                    listOfPolygonPoints.last().y.toFloat(),
                    mPolygonPaint
                )
            }

            if (!listOfLinesPoints.isNullOrEmpty()) {
                val startingValue = listOfLinesPoints[0]
                mPathLine.moveTo(startingValue.x.toFloat(), startingValue.y.toFloat())
                for (point in listOfLinesPoints) {
                    mPathLine.lineTo(point.x.toFloat(), point.y.toFloat())
                    canvas.drawCircle(point.x.toFloat(), point.y.toFloat(), zoomlvl, mLinePaint)
                }
            }

            if (!listOfPointsPoints.isNullOrEmpty()) {
                for (point in listOfPointsPoints) {
                    canvas.drawCircle(point.x.toFloat(), point.y.toFloat(), zoomlvl, mPointPaint)
                }
            }

            canvas.drawPath(mPathPolygon, mPolygonPaint)
            canvas.drawPath(mPathLine, mLinePaint)
        }
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        performClick()
        when (action) {
            DRAW_POLYGON -> addPointToPolygon(
                Point(
                    event!!.x.toInt(),
                    event.y.toInt()
                )
            )
            DRAW_LINE -> addPointToLine(Point(event!!.x.toInt(), event.y.toInt()))
            DRAW_POINT -> addPointToPointsList(
                Point(
                    event!!.x.toInt(),
                    event.y.toInt()
                )
            )
        }
        selectedX = event!!.x
        selectedY = event.y
        return super.onTouchEvent(event)
    }

    private fun addPointToPolygon(point: Point) {
        listOfPolygonPoints.add(point)
        invalidate()
    }

    private fun addPointToLine(point: Point) {
        listOfLinesPoints.add(point)
        invalidate()
    }

    fun addPointToPointsList(point: Point) {
        listOfPointsPoints.add(point)
        invalidate()
    }

    fun clearPointToPointsList() {
        listOfLinesPoints.clear()
        invalidate()
    }

    fun setCurrentAction(newAction: Enums.Actions) {
        action = newAction
    }

    fun setZoom(zoomLevel2: Float){
        zoomLevel = zoomLevel2
        invalidate()
    }

}