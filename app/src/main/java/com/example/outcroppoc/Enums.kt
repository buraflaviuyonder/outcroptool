package com.example.outcroppoc

object Enums {
    enum class Actions{
        DRAW_POLYGON,
        DRAW_LINE,
        DRAW_POINT,
        MEASURE
    }
}